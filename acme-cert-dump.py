#!/usr/bin/env python

import argparse
import base64
import json
import os
import shlex
import subprocess
import sys
import re
import time

char_ok      ='✓'
char_error   ='✗'
char_wait    ='⌚'
char_step    ='├'
char_substep ='│'
char_last    ='└'

def main(raw_args=sys.argv[1:]):
    parser = argparse.ArgumentParser(
        description="Grab certificates out of Traefik's acme.json file")
    parser.add_argument('acme_json',
        help='path to the acme.json file')
    parser.add_argument('dest_dir',
        help='path to the directory to store the certificate')
    parser.add_argument('--domain_resolver', required=False,
        help='name of the domain resolver')
    parser.add_argument('--domain', required=False,
        help='domain to get certificate for')
    parser.add_argument('--post-update', required=False,
        help='command to run after updating the certificate')
    parser.add_argument('--refresh', required=False,
        help='refresh dump of acme.json file every REFRESH seconds')

    args = parser.parse_args(raw_args)

    dump_certs(args)

    if args.refresh:
        refreshDelay = int(args.refresh)
        if refreshDelay > 0:
            while True:
                print(char_wait+" Refresh in "+str(refreshDelay)+" seconds...")
                time.sleep(refreshDelay)
                dump_certs(args)

def dump_certs(args):
    if os.stat(args.acme_json).st_size == 0:
        print(char_error+" "+args.acme_json+" file is empty")
        return
    
    try:
        acme_json = read_acme_json(args.acme_json)
        if args.domain_resolver and args.domain:
            print("Extracting certificates for domain %s of resolver %s" % (args.domain, args.domain_resolver,))
            try:
                dump_domain_certs(acme_json, args.domain_resolver, args.domain, args.dest_dir+"/"+args.domain_resolver+"/"+args.domain, args.post_update)
            except Exception as error:
                print_step_closure("Unable to dump certificates. %s: %s" % (format(type(error).__name__),format(error),), 1)
        elif args.domain_resolver and not args.domain:
            print("Extracting certificates for all domains of resolver %s" % (args.domain_resolver,))
            dump_all_domain_certs(acme_json, args.domain_resolver, args.dest_dir+"/"+args.domain_resolver, args.post_update)
        elif not args.domain_resolver and args.domain:
            print("Extracting certificates for domain %s of all resolvers" % (args.domain))
            dump_all_resolver_domain_certs(acme_json, args.domain, args.dest_dir, args.post_update)
        else:
            print("Extracting certificates for all resolvers")
            dump_all_resolver_all_domain_certs(acme_json, args.dest_dir, args.post_update)
    except FileNotFoundError as error:
        print(error.strerror+": '"+args.acme_json+"'")
        return
    
    print_step_closure(char_ok+' Dump done')

def dump_all_resolver_domain_certs(acme_json, domain, dest_dir, post_update, level=0):
    domain_resolvers = read_domain_resolvers(acme_json)
    if len(domain_resolvers) == 0:
        print_step_message(char_error+" No resolver found", level)
        return
    for domain_resolver in domain_resolvers:
        print_step_message("Dumping resolver %s" % (domain_resolver,), level)
        try:
            dump_domain_certs(acme_json, domain_resolver, domain, dest_dir+"/"+domain_resolver+"/"+domain, post_update, level+1)
        except RuntimeError:
            print_step_closure("No certificate to dump", level+2)
        except Exception as error:
            print_step_closure("Unable to dump certificates. %s: %s" % (format(type(error).__name__),format(error),), level+1)
        print_step_closure(char_ok+' Resolver done', level+1)

def dump_all_resolver_all_domain_certs(acme_json, dest_dir, post_update, level=0):
    domain_resolvers = read_domain_resolvers(acme_json)
    if len(domain_resolvers) == 0:
        print_step_message(char_error+" No resolver found", level)
        return
    for domain_resolver in domain_resolvers:
        try:
            print_step_message("Dumping resolver %s" % (domain_resolver,), level)
            dump_all_domain_certs(acme_json, domain_resolver, dest_dir+"/"+domain_resolver, post_update, level+1)
        except KeyError as error:
            print_step_message(char_error+" No certificate for resolver %s. %s: %s" % (domain_resolver,format(type(error).__name__),format(error),), level)
        except Exception as error:
            print_step_message(char_error+" Unable to dump resolver %s. %s: %s" % (domain_resolver,format(type(error).__name__),format(error),), level)

def dump_all_domain_certs(acme_json, domain_resolver, dest_dir, post_update, level=0):
    try:
        domains = read_domains(acme_json, domain_resolver)
    except Exception as error:
        print_step_message(char_error+" Unable to dump resolver certificates. %s: %s" % (format(type(error).__name__),format(error),), level)
        return
    for domain in domains:
        try:
            dump_domain_certs(acme_json, domain_resolver, domain, dest_dir+"/"+domain, post_update, level)
        except Exception as error:
            print_step_message(char_error+" Unable to dump certificates. %s: %s" % (format(type(error).__name__),format(error),), level)
    if level > 0:
        print_step_closure(char_ok+' Resolver done', level)

def dump_domain_certs(acme_json, domain_resolver, domain, dest_dir, post_update, level=0):
    print_step_message('Dumping domain %s' % (domain,), level)
    new_privkey, new_fullchain, new_cert, new_chain = read_domain_certs(acme_json, domain_resolver, domain)

    old_privkey = read_cert(dest_dir, 'privkey.pem')
    old_fullchain = read_cert(dest_dir, 'fullchain.pem')
    old_cert = read_cert(dest_dir, 'cert.pem')
    old_chain = read_cert(dest_dir, 'chain.pem')

    if new_privkey != old_privkey or new_fullchain != old_fullchain or new_cert != old_cert or new_chain != old_chain:
        print_step_message('Certificates changed! Writing new files...', level+1)
        if not os.path.exists(dest_dir):
            os.makedirs(dest_dir)
        write_cert(dest_dir, 'privkey.pem', new_privkey)
        write_cert(dest_dir, 'fullchain.pem', new_fullchain)
        write_cert(dest_dir, 'cert.pem', new_cert)
        write_cert(dest_dir, 'chain.pem', new_chain)

        if post_update is not None:
            print_step_message('Running post update command "%s"' % (post_update,), level+1)
            post_update(post_update)
        print_step_closure(char_ok+' Domain done', level+1)
    else:
        print_step_closure('Certificates unchanged. Skipping...', level+1)

def print_step_message(message, level=0):
    print_step(char_step, message, level)

def print_step_closure(message, level=0):
    print_step(char_last, message, level)

def print_step(prefix, message, level):
    level_prefix=''
    for x in range(level):
        level_prefix+=char_substep+" "
    print(level_prefix+prefix+' '+message)

def read_acme_json(acme_json_path):
    with open(acme_json_path) as acme_json_file:
        acme_json = json.load(acme_json_file)
    return acme_json

def read_cert(storage_dir, filename):
    cert_path = os.path.join(storage_dir, filename)
    if os.path.exists(cert_path):
        with open(cert_path) as cert_file:
            return cert_file.read()
    return None

def write_cert(storage_dir, filename, cert_content):
    cert_path = os.path.join(storage_dir, filename)
    with open(cert_path, 'w') as cert_file:
        cert_file.write(cert_content)
    os.chmod(cert_path, 0o600)

def read_domain_resolvers(acme_json):
    domain_resolvers = [key for key in acme_json.keys()]
    return domain_resolvers

def read_domains(acme_json, domain_resolver):
    certs_json = acme_json[domain_resolver]['Certificates']
    domain_certs = [cert['domain']['main'] for cert in certs_json]
    return domain_certs

def read_domain_certs(acme_json, domain_resolver, domain):
    certs_json = acme_json[domain_resolver]['Certificates']
    domain_certs = [cert for cert in certs_json
                    if cert['domain']['main'] == domain]
    if not domain_certs:
        raise RuntimeError(
            'Unable to find certificate for domain "%s"' % (domain,))
    elif len(domain_certs) > 1:
        raise RuntimeError(
            'More than one (%d) certificates for domain "%s"' % (len(domain_certs), domain,))

    [domain_cert] = domain_certs
    privkey = base64.b64decode(domain_cert['key']).decode('utf-8')
    fullchain = base64.b64decode(domain_cert['certificate']).decode('utf-8')
    splitchain = re.split("-----END CERTIFICATE-----\n+-----BEGIN CERTIFICATE-----", fullchain)
    cert = splitchain[0]+"-----END CERTIFICATE-----"
    chain = "-----BEGIN CERTIFICATE-----"+splitchain[1]
    return (privkey,
            fullchain,
            cert,
            chain)

def post_update(command):
    subprocess.check_call(shlex.split(command))

if __name__ == '__main__':
    main()