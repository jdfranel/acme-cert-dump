# ACME Cert Dump

Python script that converts the Traefik acme.json file containing the certificates into separate .pem files

## Usage

Most basic usage

```sh
usage: acme-cert-dump.py [-h] [--domain_resolver DOMAIN_RESOLVER] [--domain DOMAIN] [--post-update POST_UPDATE] [--refresh REFRESH] acme_json dest_dir
```

*acme_json*: path to the acme.json file\
*dest_dir*: path to the directory to store the certificate

Available OPTIONS are:

`--domain_resolver DOMAIN_RESOLVER`: name of the domain resolver to get certificates for\
`--domain DOMAIN`: domain to get certificates for\
`--post-update POST_UPDATE`: command to run after updating the certificate\
`--refresh REFRESH`: refresh dump of acme.json file every REFRESH seconds

## Result

When provided with an acme.json file contining:

```json
{
  "cert-resolver": {
    ...
    "Certificates": [
      {
        "domain": {
          "main": "my.domain"
        },
        "certificate": "ABCD....",
        "key": "ABCD....",
        "Store": "default"
      },
      ...
    ],
  }
}
```

The certificates will be converted into

```txt
[output-dir]
└── [cert-resolver]
    └── [my.domain]
        ├── cert.pem
        ├── chaim.pem
        ├── fullchain.pem
        └── private.pem
```

## Credits

This script is a refactoring from the great work of [JayH5](https://gist.github.com/JayH5): [acme-cert-dump.py](https://gist.github.com/JayH5/f9e4dc48635f3faa63c52813ff6d115f)

## License

MIT

## Donate

If this tool has allowed you to spare a fair amount of time handling your database backups, please consider to offer me a coffee or, even better, a beer.

* [Buy me a coffee](https://www.buymeacoffee.com/oT35gfqDv)
* BTC : bc1q4v4rechtedld2zl5g080vj0flryklanyehyt30
* ETH : 0x98A943aC7D8148e746b549E512F5ACfdc91939dC
* LTC : ltc1qk4f4tg3aqavrguqvu05kfa86q294nm3s5k3g40
* XRP : rPqjg6474S9frY13Tw5eugF6aRowFEiR5Q
* XLM : GBPUYXF6UWWIJXS7AXTDRHVM4OL2ZSROJAKVCPDG5EDX6PFVIG6UVVJH
