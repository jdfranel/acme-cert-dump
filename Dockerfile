FROM python:3-alpine

ADD acme-cert-dump.py /

VOLUME [ "/acme" ]
VOLUME [ "/dump" ]

ENTRYPOINT [ "python3", "./acme-cert-dump.py" ]