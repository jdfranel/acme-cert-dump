#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
BASE_PATH=$SCRIPTPATH/certs

set -e

# https://superuser.com/a/1541780

ROOT_DIRECTORY=$BASE_PATH/root
init_root_ca() {
  mkdir -p $ROOT_DIRECTORY
  # Generate Root certifiacte
  if [ ! -f $ROOT_DIRECTORY/privkey.pem ] && [ ! -f $ROOT_DIRECTORY/cert.pem ]; then
    echo "Generating root certificate"
    openssl req \
      -newkey rsa:4096 \
      -keyout $ROOT_DIRECTORY/privkey.pem \
      -x509 \
      -new \
      -nodes \
      -out $ROOT_DIRECTORY/cert.pem \
      -days 3650 \
      -subj "/C=CH/ST=Zurich/L=Zurich/O=ACME/OU=Test/CN=Root CA"

      # Check root CA
      # openssl x509 \
      #   -in $ROOT_DIRECTORY/cert.pem \
      #   -text \
      #   -noout
  fi
}

generate_certificates() {
  DOMAIN=$1
  DOMAIN_DIRECTORY=$BASE_PATH/$DOMAIN
  mkdir -p $DOMAIN_DIRECTORY
  # Generate CSR
  echo "Generating $DOMAIN certificate signing request"
  openssl req \
    -newkey rsa:4096 \
    -keyout $DOMAIN_DIRECTORY/privkey.pem \
    -new \
    -nodes \
    -out $DOMAIN_DIRECTORY/csr.pem \
    -subj "/C=CH/ST=Zurich/L=Zurich/O=ACME/OU=Test/CN=${DOMAIN}"
  # Generate Certificate
  echo "Generating $DOMAIN certificate"
  openssl x509 \
    -req \
    -in $DOMAIN_DIRECTORY/csr.pem \
    -CA $ROOT_DIRECTORY/cert.pem \
    -CAkey $ROOT_DIRECTORY/privkey.pem \
    -CAcreateserial \
    -out $DOMAIN_DIRECTORY/cert.pem \
    -days 365
  # openssl x509 \
  #   -in $DOMAIN_DIRECTORY/cert.pem \
  #   -text \
  #   -noout

  cat $DOMAIN_DIRECTORY/cert.pem $ROOT_DIRECTORY/cert.pem > $DOMAIN_DIRECTORY/fullchain.pem
  cp $ROOT_DIRECTORY/cert.pem $DOMAIN_DIRECTORY/chain.pem
}

init_root_ca

# Generate certs by cert resolvers
LE_HTTP_DOMAINS=(alice.test.domain bob.test.domain)
for DOMAIN in ${LE_HTTP_DOMAINS[@]}; do
  generate_certificates $DOMAIN
done

LE_TLS_DOMAINS=(charlie.test.domain dave.test.domain)
for DOMAIN in ${LE_TLS_DOMAINS[@]}; do
  generate_certificates $DOMAIN
done

LE_DNS_DOMAINS=(ethan.test.domain)
for DOMAIN in ${LE_DNS_DOMAINS[@]}; do
  generate_certificates $DOMAIN
done

print_json_cert_resolver() {
  CERT_RESOLVER=$1
  DOMAINS=("${@:2}")
  echo "Inserting $CERT_RESOLVER domains: $DOMAINS"
  JSON="$JSON
    \t\"$CERT_RESOLVER\": {\n
    \t\t\"Certificates\": [\n"

  FIRST_DOMAIN=1
  for DOMAIN in ${DOMAINS[@]}; do
    DOMAIN_DIRECTORY=$BASE_PATH/$DOMAIN
    if [ $FIRST_DOMAIN == 1 ]; then
      FIRST_DOMAIN=0
    else
      JSON="$JSON,"
    fi
    JSON="$JSON
    \t\t\t{\n
    \t\t\t\t\"domain\": {\n
    \t\t\t\t\t\"main\": \"$DOMAIN\"\n
    \t\t\t\t},\n
    \t\t\t\t\"certificate\": \"$(cat $DOMAIN_DIRECTORY/fullchain.pem | base64 | tr -d '\n')\",\n
    \t\t\t\t\"key\": \"$(cat $DOMAIN_DIRECTORY/privkey.pem | base64 | tr -d '\n')\",\n
    \t\t\t\t\"store\": \"default\"\n
    \t\t\t}"
  done
  JSON="$JSON\n
  \t\t]\n
  \t}"
}

# Generate json
echo "Generating acme.json file"
JSON="{\n"

print_json_cert_resolver "lets-encrypt-http" "${LE_HTTP_DOMAINS[*]}"
JSON="$JSON,\n"
print_json_cert_resolver "lets-encrypt-tls" "${LE_TLS_DOMAINS[*]}"
JSON="$JSON,\n"
print_json_cert_resolver "lets-encrypt-dns" "${LE_DNS_DOMAINS[*]}"

JSON="$JSON\n}"

echo -e $JSON > $BASE_PATH/acme.json