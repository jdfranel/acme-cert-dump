#!/bin/bash

SCRIPTPATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

set -e

mkdir -p "$SCRIPTPATH/output"
mkdir -p "$SCRIPTPATH/certs"

check_resolver_certs() {
  local CERT_RESOLVER=$1
  local DOMAIN=$2
  local CERT_PATH="output/$CERT_RESOLVER/$DOMAIN"
  check_certs "$CERT_PATH" "$DOMAIN"
}

check_certs() {
  local CERT_PATH=$1
  local DOMAIN=$2

  if [[ $(cat "$SCRIPTPATH/certs/$DOMAIN/fullchain.pem") != $(cat "$SCRIPTPATH/$CERT_PATH/fullchain.pem") ]]; then
    echo "$DOMAIN fullchain mismatch"
    exit 1
  fi
  if [[ $(cat "$SCRIPTPATH/certs/$DOMAIN/privkey.pem") != $(cat "$SCRIPTPATH/$CERT_PATH/privkey.pem") ]]; then
    echo "$DOMAIN privkey mismatch"
    exit 1
  fi
  if [[ $(cat "$SCRIPTPATH/certs/$DOMAIN/cert.pem") != $(cat "$SCRIPTPATH/$CERT_PATH/cert.pem") ]]; then
    echo "$DOMAIN cert mismatch"
    exit 1
  fi
  if [[ $(cat "$SCRIPTPATH/certs/$DOMAIN/chain.pem") != $(cat "$SCRIPTPATH/$CERT_PATH/chain.pem") ]]; then
    echo "$DOMAIN chain mismatch"
    exit 1
  fi
}

# generating test data set
sh -c "$SCRIPTPATH/generate_certs.sh"

LE_HTTP_DOMAINS=(alice.test.domain bob.test.domain)
LE_TLS_DOMAINS=(charlie.test.domain dave.test.domain)
LE_DNS_DOMAINS=(ethan.test.domain)

# export all
echo "[TEST] Export all"
python3 "$SCRIPTPATH/../acme-cert-dump.py" "$SCRIPTPATH/certs/acme.json" "$SCRIPTPATH/output"

for DOMAIN in "${LE_HTTP_DOMAINS[@]}"; do
  check_resolver_certs "lets-encrypt-http" "$DOMAIN"
done

for DOMAIN in "${LE_TLS_DOMAINS[@]}"; do
  check_resolver_certs "lets-encrypt-tls" "$DOMAIN"
done

for DOMAIN in "${LE_DNS_DOMAINS[@]}"; do
  check_resolver_certs "lets-encrypt-dns" "$DOMAIN"
done
echo "[TEST] OK"
rm -rf "$SCRIPTPATH"/output/*

# export one cert resolver
echo "[TEST] Export only lets-encrypt-http"
python3 "$SCRIPTPATH/../acme-cert-dump.py" --domain_resolver=lets-encrypt-http "$SCRIPTPATH/certs/acme.json" "$SCRIPTPATH/output"

for DOMAIN in "${LE_HTTP_DOMAINS[@]}"; do
  check_resolver_certs "lets-encrypt-http" "$DOMAIN"
done

if [[ -d $SCRIPTPATH/output/lets-encrypt-tls ]]; then
    echo "lets-encrypt-tls should not exist"
    exit 1
fi

if [[ -d $SCRIPTPATH/output/lets-encrypt-dns ]]; then
    echo "lets-encrypt-dns should not exist"
    exit 1
fi
echo "[TEST] OK"
rm -rf "$SCRIPTPATH"/output/*

# export one domain
echo "[TEST] Export only charlie.test.domain"
python3 "$SCRIPTPATH/../acme-cert-dump.py" --domain=charlie.test.domain "$SCRIPTPATH/certs/acme.json" "$SCRIPTPATH/output"

check_resolver_certs "lets-encrypt-tls" "charlie.test.domain"

if [[ -d $SCRIPTPATH/output/lets-encrypt-tls/dave.test.domain ]]; then
    echo "lets-encrypt-tls/dave.test.domain should not exist"
    exit 1
fi

if [[ -d $SCRIPTPATH/output/lets-encrypt-http ]]; then
    echo "lets-encrypt-http should not exist"
    exit 1
fi

if [[ -d $SCRIPTPATH/output/lets-encrypt-dns ]]; then
    echo "lets-encrypt-dns should not exist"
    exit 1
fi
echo "[TEST] OK"
rm -rf "$SCRIPTPATH"/output/*


# export one domain
echo "[TEST] Export only lets-encrypt-tls charlie.test.domain"
python3 "$SCRIPTPATH/../acme-cert-dump.py" --domain_resolver=lets-encrypt-tls --domain=charlie.test.domain "$SCRIPTPATH/certs/acme.json" "$SCRIPTPATH/output"

check_resolver_certs "lets-encrypt-tls" "charlie.test.domain"

if [[ -d $SCRIPTPATH/output/lets-encrypt-tls/dave.test.domain ]]; then
    echo "lets-encrypt-tls/dave.test.domain should not exist"
    exit 1
fi

if [[ -d $SCRIPTPATH/output/lets-encrypt-http ]]; then
    echo "lets-encrypt-http should not exist"
    exit 1
fi

if [[ -d $SCRIPTPATH/output/lets-encrypt-dns ]]; then
    echo "lets-encrypt-dns should not exist"
    exit 1
fi
echo "[TEST] OK"
rm -rf "$SCRIPTPATH"/output/*

echo "[TEST] All test passed !"

rm -rf "$SCRIPTPATH/certs"
rm -rf "$SCRIPTPATH/output"